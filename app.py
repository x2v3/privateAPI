import json
from pypinyin import pinyin as py
import pypinyin
from flask import Flask,request
from network import create_blueprint as create_network_blueprint

app=Flask(__name__)
app.register_blueprint(create_network_blueprint())

def shengmu(characters):
    return py(characters,style=pypinyin.FIRST_LETTER,errors='ignore')

@app.route('/pinyin', methods=['GET', 'POST'])
def pinyin():
    chars=request.args.get('chars','')
    r=shengmu(chars)
    return json.dumps(r),200,{"Access-Control-Allow-Origin":"*"}

@app.route('/pinyin_batch', methods=['GET','POST'])
def pinyin_batch():
    req=request.args.get('chars','[]')
    req_json=json.loads(req)
    result_dict={}
    for c in req_json:
        result_dict[c]=shengmu(c)
    return json.dumps(result_dict),200,{"Access-Control-Allow-Origin":"*"}
    

if __name__ == '__main__':
    app.run('127.0.0.1', 2233, debug=True)

