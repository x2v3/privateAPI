import bencode
from flask import Blueprint, session
import tempfile


def torrent2magnet(torrentfile):
    torrent = open(torrentfile, 'r').read()
    metadata = bencode.bdecode(torrent)
    hashcontents = bencode.bencode(metadata['info'])
    import hashlib
    digest = hashlib.sha1(hashcontents).digest()
    import base64
    b32hash = base64.b32encode(digest)
    return 'magnet:?urn:btih:%s' % b32hash


def magnet2torrent(magnet):
    pass


def create_blueprint():
    bp = Blueprint()

    @bp.route('t2m', methods=['GET', 'POST'])
    def t2m():
        torrent = session.get('torrent')
        torrent_file= tempfile.NamedTemporaryFile()
        torrent_file.write(torrent)
        torrent_file.close()
        magnet = torrent2magnet(torrent_file.name)
        return magnet

    # @bp.route('m2t', methods=['GET', 'POST'])
    def m2t():
        magnet = session.get('magnet')
        torrent = magnet2torrent(magnet)
        pass

    return bp
